/* eslint-disable no-use-before-define */
/* eslint-disable consistent-return */
async function vconsole(str, title = '') {
    const clone = JSON.parse(JSON.stringify(str))
    try {
        console.time('截取长字符')
        if (!['object', 'array', 'string'].includes(_type(clone))) return clone
        if (_type(clone) === 'object') _object(clone)
        if (_type(clone) === 'array') _array(clone)
        console.log(`${title}-->`, clone)
            // console.timeEnd('截取长字符')
    } catch (err) {
        console.log(`${title}-->`, 'vconsolejs错误')
    }
}

// 字符截断
function _replace(str) {
    if (str.length < 20) return str
    return `${str.substring(0, 20)}***`
}

// 类型判断
function _type(str) {
    return typeof str !== 'object' ?
        typeof str :
        Object.prototype.toString.call(str).slice(8, -1).toLowerCase()
}

// 对象处理
function _object(str) {
    for (const key in str) {
        str[key] = common(str[key])
    }
}

// 数组处理
function _array(str) {
    for (let index = 0; index < str.length; index++) {
        str[index] = common(str[index])
    }
}

// 公共逻辑
function common(data) {
    if (_type(data) === 'string') {
        data = _replace(data)
    }
    if (_type(data) === 'object') _object(data)
    if (_type(data) === 'array') _array(data)
    return data
}

export { _type, _object, _array, vconsole }